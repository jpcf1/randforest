CC=g++
CPPFLAGS=-Wall

test_tree: build/test_tree.o build/Dataset.o build/Node.o build/Tree.o
	$(CC) $(CPPFLAGS) -o bin/test_tree build/test_tree.o build/Dataset.o build/Node.o build/Tree.o

test_dataset: build/test_dataset.o build/Dataset.o build/Node.o build/Tree.o
	$(CC) $(CPPFLAGS) -o bin/test_dataset build/test_dataset.o build/Dataset.o build/Node.o build/Tree.o

build/test_tree.o: src/test/src/test_tree.cpp  src/lib/include/Node.h src/lib/include/Tree.h
	$(CC) $(CPPFLAGS) -g -c src/test/src/test_tree.cpp -o build/test_tree.o

build/test_dataset.o: src/test/src/test_dataset.cpp src/lib/include/Dataset.h 
	$(CC) $(CPPFLAGS) -c src/test/src/test_dataset.cpp -o build/test_dataset.o

build/Dataset.o: src/lib/include/Dataset.h
	$(CC) $(CPPFLAGS) -c src/lib/src/Dataset.cpp -o build/Dataset.o

build/Node.o: src/lib/include/Node.h
	$(CC) $(CPPFLAGS) -c src/lib/src/Node.cpp -o build/Node.o

build/Tree.o: src/lib/include/Tree.h
	$(CC) $(CPPFLAGS) -c src/lib/src/Tree.cpp -o build/Tree.o

clean:
	rm bin/*
	rm build/*

run_dataset:
	./bin/test_dataset

run_tree:
	./bin/test_tree

debug_tree:
	gdb ./bin/test_tree
