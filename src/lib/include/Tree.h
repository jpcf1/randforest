#include "../include/Node.h"

#define RIGHT_BRANCH 1
#define LEFT_BRANCH  0

class Tree {

    private:
        // Pointer to the tree root 
        Node* root;

        // Pointer to the current node;
        Node* next_node;
        
        // Tree characteristics
        int depth;

    public:
        // Default constructor, allocates a root node. 'next_node' points to root by default
        Tree () {root = new Node(); next_node = root; depth=1;}

        // Adds a node to the node pointed by next_node, in the desired 'dir' place:  '1' for right, '0' for left
        bool add_node(int label, int dir);

        // Advances the 'next_node' pointer in the desired direction. '1' for right, '0' for left
        bool dive_node(int dir);

        // Takes 'next_node' back to the root
        void go_root_node(void);

        // Returns a pointer to the 'next_node'
        Node* get_next_node(void);

        // Prints the tree, DFS in-order
        void print_dfs(void);

        // Prints the tree, BFS in-order
        void print_bfs(void);



        
};

