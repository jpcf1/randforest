#include <stdio.h>

class Node {

    private:
        // Pointers to other nodes
        Node* l;
        Node* r;

        // If a leaf node, this label different than -1 (start value)
        int label;

        // Trainable parameters
        int   feature_idx;
        int   branch_test_type;
        float split_point;

    public:
        // Default constructor
        Node () {label=-1; l = NULL; r = NULL; feature_idx=-1; branch_test_type=-1; split_point=-1;}

        // Get node pointers
        Node* get_node_r(void);
        Node* get_node_l(void);

        // Set node pointers
        bool  set_node_r(Node* new_node);
        bool  set_node_l(Node* new_node);

        // Branching test function
        bool branch_test_func(float* features);

        // Traverse the node. Returns pointer to next node
        Node* traverse(float* features);

        // Prints the Node full info
        void print(void);

        int get_label(void);

        void set_label(int new_label);

};
