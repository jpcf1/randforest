#include <vector>

class Dataset {

    private:
        std::vector< std::vector<float> > features;
        std::vector<int> labels;

        int N;
        int D;

    public:
        // Default constructor
        Dataset (int dimensionality) {N=0; D=dimensionality;}
        
        // Other methods
        void insert_tr_ex(std::vector<float> feature_ex, int label_ex);
        bool import_dataset(const char* filepath, const char* line_format);
        void print(void);
};

