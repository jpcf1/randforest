#include "../include/Node.h"
#include <stdio.h>

bool Node::branch_test_func(float* features) {
   
    if(branch_test_type == 0) {
        if(features[feature_idx] < split_point)
            return true;
        else
            return false;
    }
    else if(branch_test_type == 1) {
        if(features[feature_idx] >= split_point)
            return true;
        else
            return false;
    }
    else {
        fprintf(stderr, "ERROR, Node::branch_test_func(): invalid test type\n");
        return false;
    }
}

Node* Node::traverse(float* features) {
    if (Node::branch_test_func(features))
        return r;
    else
        return l;
}

Node* Node::get_node_r(void) {
    return r;
}


Node* Node::get_node_l(void) {
    return l;
}

bool Node::set_node_r(Node* new_node) {
    if(NULL != new_node) {
        r = new_node;
        return true;
    }
    else {
        fprintf(stderr, "Error while linking right node: pointer is NULL\n");
        return false;
    }
}

bool Node::set_node_l(Node* new_node) {
    if(NULL != new_node) {
        l = new_node;
        return true;
    }
    else {
        fprintf(stderr, "Error while linking left node: pointer is NULL\n");
        return false;
    }
}

void Node::print(void) {
    fprintf(stderr, "Feature Index chosen for test: %d\nSplit point chosen for test: %f\nBranch test type: %d\nLabel: %d\n",
            feature_idx, split_point, branch_test_type, label);
    fprintf(stderr, "Left leaf: %p\nRight leaf: %p\n", l, r);
}

int Node::get_label(void) {
    return label;
}

void Node::set_label(int new_label) {
    label = new_label;
}


