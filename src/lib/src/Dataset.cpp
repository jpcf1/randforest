#include "../include/Dataset.h"
#include <stdio.h>

void Dataset::insert_tr_ex(std::vector<float> feature_ex, int label_ex) {
    features.push_back(feature_ex);
    labels.push_back(label_ex);
    N++;
}

bool Dataset::import_dataset(const char* file_path, const char* line_format) {
    
    FILE* fd_ds;
    std::vector<float> features(9);
    int label;

    fd_ds = fopen(file_path, "r");

    if(NULL == fd_ds) {
        fprintf(stderr, "Error while opening file at %s...\n", file_path);
        return false;
    }

    while (11 == fscanf(fd_ds, line_format, &label, &features[0], &features[1], &features[2], &features[3], &features[4], &features[5],&features[6], &features[7], &features[8], &label)) {
        insert_tr_ex(features, label);
    }
    
    fprintf(stderr, "Succesfully read %d Training Examples", N);
    return true;
}

void Dataset::print(void) {
    for(int i=0; i < N; i++) {
        for(int j=0; j < D; j++) {
            printf("%f, ", features[i][j]);
        }
        printf("LABEL: %d\n", labels[i]);
    }
    fprintf(stderr, "Training Examples: %d\nFeature Dimensionality: %d \n\n", N, D);
}

