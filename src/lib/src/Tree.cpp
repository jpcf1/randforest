#include "../include/Tree.h"
#include <stdio.h>

bool Tree::add_node(int label, int dir) {
    
    if(LEFT_BRANCH == dir) {
        Node* tmp = new Node();
        tmp->set_label(label);
        next_node->set_node_l(tmp);
    }
    else if (RIGHT_BRANCH == dir) {
        Node* tmp = new Node();
        tmp->set_label(label);
        next_node->set_node_r(tmp);
    }
    else
        return false;

    return true;
}

bool Tree::dive_node(int dir) {
    if(LEFT_BRANCH == dir) {
        if(next_node->get_node_l() != NULL) {
            next_node = next_node->get_node_l();
        }
        else {
            fprintf(stderr, "We are at a leaf node, cannot dive further\n");
            return false;
        }
    }
    else if (RIGHT_BRANCH == dir) {
        if(next_node->get_node_r() != NULL) {
            next_node = next_node->get_node_r();
        }
        else {
            fprintf(stderr, "We are at a leaf node, cannot dive further\n");
            return false;
        }
    }
    else
        return false;

    return true;
}

void  Tree::go_root_node(void) {
    next_node = root;    
}

Node* Tree::get_next_node(void) {
   return next_node; 
}

void Tree::print_dfs(void) {

}

void Tree::print_bfs(void) {

}

