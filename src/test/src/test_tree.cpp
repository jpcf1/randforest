#include <stdio.h>
#include "../../lib/include/Tree.h"

int main() {
    
    // The Tree object
    Tree  tree;

    // Populate the tree
    tree.add_node(2, LEFT_BRANCH);
    tree.add_node(3, RIGHT_BRANCH);
    tree.dive_node(LEFT_BRANCH);
    tree.add_node(4, LEFT_BRANCH);
    tree.add_node(5, RIGHT_BRANCH);
    tree.go_root_node();
    tree.dive_node(RIGHT_BRANCH);
    tree.add_node(6, LEFT_BRANCH);
    tree.add_node(7, RIGHT_BRANCH);

    return 0;

}
