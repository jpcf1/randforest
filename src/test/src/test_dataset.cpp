#include <stdio.h>
#include "../../lib/include/Dataset.h"

int main() {
    
    // The Dataset object
    Dataset ds_glass(9);

    // READING AND PARSING DATASET FROM INPUT FILE 
    ds_glass.import_dataset("src/test/data/glass.data", "%d,%f,%f,%f,%f,%f,%f,%f,%f,%f,%d\n");
    ds_glass.print();

}
